﻿"use strict";

function showProjectPage(details) {
  sessionStorage.setItem("selected-project-details", JSON.stringify(details));
  window.open("preview.html", "_self");
}

function passwordCheck() {
  let query = document.location.hash.substring(1);

  // immediately change the hash
  document.location.hash = '';

  // parse it in some reasonable manner ... 
  let params = {};
  let parts = query.split(/&/);
  for (let i in parts) {
    let t = parts[i].split(/=/);
    params[decodeURIComponent(t[0])] = decodeURIComponent(t[1]);
  }
  if ("pw" in params) {
    let insert = document.getElementById("insert");

    let input = document.createElement("input");
    input.classList.add("shown");
    input.id = "wip";
    input.type = "radio";
    input.name = "API";

    let label = document.createElement("label");
    label.classList.add("shown");
    label.htmlFor = "wip";
    label.innerHTML = "WIP";

    insert.insertAdjacentElement('afterend', label);
    insert.insertAdjacentElement('afterend', input);



    input = document.createElement("input");
    input.classList.add("shown");
    input.id = "experimental";
    input.type = "radio";
    input.name = "API";

    label = document.createElement("label");
    label.classList.add("shown");
    label.htmlFor = "experimental";
    label.innerHTML = "Experimental";

    insert.insertAdjacentElement('afterend', label);
    insert.insertAdjacentElement('afterend', input);
  }
}

window.addEventListener('DOMContentLoaded', function () {
  passwordCheck();

  let xmlhttp = new XMLHttpRequest();
  let url = "projects.json";

  xmlhttp.onreadystatechange = function () {
    if (this.readyState == 4 && this.status == 200) {
      let myArr = JSON.parse(this.responseText);
      myFunction(myArr);
    }
  };
  xmlhttp.open("GET", url, true);
  xmlhttp.send();

  function myFunction(arr) {
    let counts = [];
    counts["all"] = 0;

    let projectsDiv = document.getElementById("projects");

    // iterate over JSON
    for (let j = 0; j < arr.length; j++) {
      if (arr[j].url == "https://www.w3schools.com/js/default.asp") {
        continue;
			}

      counts["all"]++;
      let div2 = document.createElement("div");
      let details = arr[j];

      // get the image
      let img = document.createElement("img");
      img.src = details.previewImage;
      div2.appendChild(img);

      // get the title
      let h2 = document.createElement("h2");
      h2.innerHTML = details.title;
      div2.appendChild(h2);

      // update count for category
      // and assign tags to data-projects field
      let dataProjects = "all"
      let tags = details.tags;
      for (let i = 0; i < tags.length; i++) {
        let tag = tags[i].toLowerCase().replace(/\W/g, '');

        if (counts[tag])
          counts[tag]++;
        else
          counts[tag] = 1;

        dataProjects += " " + tag;
      }
      div2.setAttribute("data-projects", dataProjects);

      let a = document.createElement("div");
      //a.href = details.url;
      a.style.display = "initial";
      a.appendChild(div2);
      a.classList.add('.main-btn');

      a.addEventListener("click", function () { showProjectPage(details); } );

      div2.classList.add("flange");

      projectsDiv.appendChild(a);
    }

    // add the counts for each type
    // show if count greater than 0
    for (let key in counts) {
      let input = document.getElementById(key);
      //let input = document.querySelector("#" + key);
      if (input == null) continue;
      input.classList.remove("hidden");
      input.classList.add("shown");

      let label = input.labels[0];
      label.classList.remove("hidden");
      label.classList.add("shown");

      label.innerHTML += " (" + counts[key] + ")";
    }

    // if "all" and "webgl" have the same count, one is redundant so remove one
    if (counts.all === counts.webgl) {
      let input = document.getElementById("webgl");
      let label = input.labels[0];
      label.parentNode.removeChild(label);
      input.parentNode.removeChild(input);
    }

    // restore last session if details available
    //console.log(performance.getEntriesByType("navigation")[0].type);

  }
});