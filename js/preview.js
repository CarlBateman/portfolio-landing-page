﻿"use strict";

function showProjectInIframe(details) {
	sessionStorage.setItem("selected-project-details", JSON.stringify(details));

	let projectFrame = document.getElementById("projectFrame");
	let projectTitle = document.getElementById("project-title");
	let projectInfo = document.getElementById("project-info");

	projectFrame.src = details.url;
	let end = projectTitle.innerHTML;
	projectTitle.innerHTML = details.title;
	projectTitle.innerHTML += end;
	if (details.hasOwnProperty("summary") && details.summary != "") {
		projectTitle.parentElement.innerHTML += "<br /><span class='project-title'>" + details.summary + "</span>";
	}

	//if (details.hasOwnProperty("info") && details.info != "") {
	//	let converter = new showdown.Converter();
	//	projectInfo.innerHTML = converter.makeHtml(details.info);
	//} else {
		let xmlhttp = new XMLHttpRequest();
		let url = "info/" + details.title.toLowerCase() + ".md";

		xmlhttp.onreadystatechange = function () {
			if (this.readyState == 4) {
				if (this.status == 200) {
					handleProjectInfo(this.responseText, details);
				} else {
					//document.getElementById("project-title-more-info").classList.add("hidden");
					handleNoProjectInfo(details);
				}
			}
		};
		xmlhttp.open("GET", url, true);
		xmlhttp.send();

		function handleProjectInfo(text, details) {
			// add title to top
			text = `#${details.title}\n\n ` + text;

			text += "\n\n---";

			// add actual link URL project page
			text += "\n\n ";
			text += `To see the ${details.title} page live click `;
			text += `[here](${details.url}).`;

			// back to landing page
			text += "<br />";
			text += `To return to the landing page click `;
			text += `[here](javascript:history.back()).`;

			let converter = new showdown.Converter();
			projectInfo.innerHTML = converter.makeHtml(text);
		}

		function handleNoProjectInfo(details) {
			// no project info file, so generate a basic default
			// add title to top
			let text = `#${details.title}\n\n `;

			let noInfo = true;

			if (details.hasOwnProperty("summary") && details.summary.length > 0) {
				text += (details.summary + "\n\n");
				noInfo = false;
			}
			if (details.hasOwnProperty("info") && details.info.length > 0) {
				text += (details.info + "\n\n");
				noInfo = false;
			}

			if (noInfo) {
				text += "\n\n";
				text += "Sorry, no additional information available.";
				text += "\n\n";
			}

			text += "\n\n---";

			// add actual link URL project page
			text += "\n\n ";
			text += `To see the ${details.title} page live click `;
			text += `[here](${details.url}).`;

			// back to landing page
			text += "<br />";
			text += `To return to the landing page click `;
			text += `[here](javascript:history.back()).`;

			let converter = new showdown.Converter();
			projectInfo.innerHTML = converter.makeHtml(text);
		}
	//}
}

function showProjectInfo() {
	document.getElementById('id01').classList.add("show-top");
	document.getElementById('id01').classList.add("show-opacity");
	document.getElementById('blah').classList.add("show");
}

function hideProjectInfo() {
	document.getElementById('blah').classList.remove("show");
	document.getElementById('id01').classList.remove("show-opacity");

	setTimeout(() => {
		document.getElementById('id01').classList.remove("show-top");
	}, 2000);
}

function breakOutOfIframe() {
	let projectFrame = document.getElementById("projectFrame");
	window.open(projectFrame.src, "_self");
}

window.addEventListener('DOMContentLoaded', function () {
	const details = JSON.parse(sessionStorage.getItem("selected-project-details"));
	if (details !== null) {
		showProjectInIframe(details);
	}
});

function addFrameSetters() {
	let elements = document.getElementsByTagName("a");
	for (let i = 0; i < elements.length; i++) {
		let isPreviewLink = elements[i].innerHTML.toLowerCase().endsWith("(preview)");
		if (isPreviewLink) {
			let src = elements[i].href;
			elements[i].addEventListener("click", function () {
				document.getElementById("projectFrame").src = src;
				return false;
			});
			elements[i].href = "#";
		}
	}
}

function setIframeSrc(id, src) {
	document.getElementsByClassName(id);
}