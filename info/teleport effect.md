﻿A teleport effect.

This required injecting shader code into Three.js' existing shaders to preserve lighting and shadows.

***Interactive***<br />
Left mouse button to rotate<br />
Right mouse button to rotate<br />
Mousewheel to zoom

**Source on Gitlab**<br />
[https://gitlab.com/CarlBateman/threejs-experiments/-/tree/master/teleport-effect](https://gitlab.com/CarlBateman/threejs-experiments/-/tree/master/teleport-effect)