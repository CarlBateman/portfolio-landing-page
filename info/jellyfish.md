﻿A bit of whimsy.

Note that this is hosted on glitch.com, so it might take a little time to re-awaken the project if dormant.

***Interactive***<br />
Left mouse button to rotate<br />
Right mouse button to rotate<br />
Mousewheel to zoom

**Source on Glitch**<br />
[https://glitch.com/~wavy-jellyfish](https://glitch.com/~wavy-jellyfish)