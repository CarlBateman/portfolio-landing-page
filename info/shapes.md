﻿Inspired by a PlayStation ident, this is intended to provide a simple background for the landing page 
that's visually interesting but not too distracting.

The set of models displayed 
(chess pieces ![chess pieces](img\knight.png), 
Three.js primitives ![img\Three.js primitives](img\dodec.png),
or random models ![random models](img\duck.png)) is rotated daily.