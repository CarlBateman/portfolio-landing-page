﻿A very early WebGL project, a submission to a Khronos "widget" competition.

As part of this project I wrote a simple animation control system.

**Source on Gitlab**<br />
[https://gitlab.com/CarlBateman/webgl-experiments/-/tree/main/webgl-experiments/widget](https://gitlab.com/CarlBateman/webgl-experiments/-/tree/main/webgl-experiments/widget)