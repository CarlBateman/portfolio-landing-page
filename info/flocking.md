﻿An implementation of the Boids flocking alrorithm.

Prompted by a contract request that sadly fell-through, the result was used as part of the 
[Algorithmic Art Season at Cornwall Museum in December 2019](https://web.archive.org/web/20201027201659/https://royalcornwallmuseum.org.uk/exhibition/algorithmic-art-season).


***Interactive***<br />
Besides the various settings in the control panel

Left mouse button to rotate<br />
Right mouse button to rotate<br />
Mousewheel to zoom

**Current working version (3D)**<br />
With a number of adjustable settings, there are three flocks (red, green and blue). 
Boids are not constrained to a volume, but rather "wraparound", passing from one boundary to that opposite.<br />
[Flocking 3D (live)](https://carlbateman.gitlab.io/Babylon-Flocking/)<br />
[Flocking 3D (preview)](https://carlbateman.gitlab.io/Babylon-Flocking/)

**Current working version (2D)**<br />
A 2D version of the above.<br />
[Flocking 2D (live)](https://carlbateman.gitlab.io/Babylon-Flocking/demo.html)<br />
[Flocking 2D (preview)](https://carlbateman.gitlab.io/Babylon-Flocking/demo.html)

**Original (flawed) version**<br />
This is an early, flawed implementation that somehow works and is shown here for the sake of completeness:<br />
[Flocking but flawed (live)](https://carlbateman.gitlab.io/Babylon-Flocking/Original/boids.html)<br />
[Flocking but flawed (preview)](https://carlbateman.gitlab.io/Babylon-Flocking/Original/boids.html)

**Source**<br />
[https://gitlab.com/CarlBateman/Babylon-Flocking/](https://gitlab.com/CarlBateman/Babylon-Flocking/)