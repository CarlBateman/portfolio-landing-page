﻿Another contract that failed to materialise. 
A simple customisable logo creator with the result being downloadble 
(PlayCanvas doesn't support download). 

Interact with the mouse, experiment with the setting on the left.


I took the opportunity to explore a few asynchronous loading techinques and to develop the same app in Three.js, Babylon.js and the PlayCanvas library.

The different engines can be selected form the drop-down.

It should be easy to add to any page, just include<br />
`  <script id="m3s-wgl-widget" src="m3s-widget-setup.js"></script>`
<br />and<br />
`  <div id="LogoGenerator"> </div>`
<br />To see it working in situ, follow the project link below.

**<span style="color:darkred">Note**</span><br/>
The Babylon.js part of the project partially broke due to breaking changes in Babylon.js. 
This was fixed by using local copies of the Babylon.js libraries.


**Source on Gitlab**<br />
[https://gitlab.com/CarlBateman/WebGL-widget-logo](https://gitlab.com/CarlBateman/WebGL-widget-logo)